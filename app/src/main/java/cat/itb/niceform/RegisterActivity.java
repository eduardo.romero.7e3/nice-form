package cat.itb.niceform;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;

public class RegisterActivity extends AppCompatActivity {

    AutoCompleteTextView pronounMenu;
    TextInputEditText username, password, repeatPassword, email, name, surnames, date;
    CheckBox tac;
    Button createAccountButton, existingAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);


        pronounMenu =  findViewById(R.id.pronounMenu);
        date = findViewById(R.id.registerBirthDateInput);
        createAccountButton = findViewById(R.id.createAccountButton);
        existingAccountButton = findViewById(R.id.existingAccountButton);
        username = findViewById(R.id.registerUserNameInput);
        password = findViewById(R.id.registerPasswordInput);
        repeatPassword = findViewById(R.id.registerRepeatPasswordInput);
        email = findViewById(R.id.registerEmailInput);
        name = findViewById(R.id.registerNameInput);
        surnames = findViewById(R.id.registerSurnameInput);
        tac = findViewById(R.id.termsAndConditionsBox);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_item, getResources().getStringArray(R.array.list_options));
        pronounMenu.setAdapter(adapter);


        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Choose A Date");
        final MaterialDatePicker<Long> picker = builder.build();

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), picker.toString());
            }
        });

        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {
                date.setText(picker.getHeaderText());
            }
        });

        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().isEmpty() ||
                        password.getText().toString().isEmpty() ||
                        repeatPassword.getText().toString().isEmpty() ||
                        email.getText().toString().isEmpty() ||
                        name.getText().toString().isEmpty() ||
                        surnames.getText().toString().isEmpty() ||
                        date.getText().toString().isEmpty() ||
                        pronounMenu.getText().toString().isEmpty() ||
                        !tac.isChecked() ||
                        password.getText().toString().equals(repeatPassword.getText().toString())
                ){
                    if(username.getText().toString().isEmpty())username.setError("Required!");
                    else username.setError(null);
                    if(password.getText().toString().isEmpty())password.setError("Required!");
                    else password.setError(null);
                    if(repeatPassword.getText().toString().isEmpty())repeatPassword.setError("Required!");
                    else repeatPassword.setError(null);
                    if(email.getText().toString().isEmpty())email.setError("Required!");
                    else email.setError(null);
                    if(name.getText().toString().isEmpty())name.setError("Required!");
                    else name.setError(null);
                    if(surnames.getText().toString().isEmpty())surnames.setError("Required!");
                    else surnames.setError(null);
                    if(date.getText().toString().isEmpty())date.setError("Required!");
                    else date.setError(null);
                    if(pronounMenu.getText().toString().isEmpty())pronounMenu.setError("Required!");
                    else pronounMenu.setError(null);
                    if(!tac.isChecked())tac.setError("Required!");
                    else tac.setError(null);
                    if(!password.getText().toString().equals(repeatPassword.getText().toString())){
                        repeatPassword.setError("Passwords don't match!");
                    }
                    else repeatPassword.setError(null);
                }
                else {
                    Intent intent = new Intent(RegisterActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                }
            }
        });

        existingAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });
    }
}
