package cat.itb.niceform;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

public class LogInActivity extends AppCompatActivity {

    Button logInButton, registerButton;
    TextInputEditText username, password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_activity);

        logInButton = findViewById(R.id.logInButton2);
        registerButton = findViewById(R.id.registerButton2);
        username = findViewById(R.id.userNameEditText);
        password = findViewById(R.id.passwordEditText);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().isEmpty() || password.getText().toString().isEmpty()){
                    if(username.getText().toString().isEmpty())username.setError("Required!");
                    else username.setError(null);

                    if(password.getText().toString().isEmpty())password.setError("Required!");
                    else password.setError(null);
                }

                else {
                    Intent intent = new Intent(LogInActivity.this, WelcomeActivity.class);
                    startActivity(intent);
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });


    }
}
